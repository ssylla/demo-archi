package fr.wis.b3.demo.bll;

import fr.wis.b3.demo.bo.Account;
import fr.wis.b3.demo.dal.DAOFactory;
import fr.wis.b3.demo.dal.IAccountDAO;
import fr.wis.b3.demo.dal.inmemory.InMemoryAccountDAO;

import java.util.Set;
public class AccountService {

	private static AccountService single;
	
	private AccountService() {}
	
	public static AccountService getSingle() {
		if (null == single) {
			single = new AccountService();
		}
		return single;
	}

	public void createAccount( Account account ) {
		IAccountDAO dao = DAOFactory.getAccountDAO();
		dao.addAccount( account );
	}
	
	public Set<Account> findAll() {
		IAccountDAO dao = DAOFactory.getAccountDAO();
		return dao.findAllAccounts();
	}

	public Account findById(long id) {
		IAccountDAO dao = DAOFactory.getAccountDAO();
		return dao.findById(id);
	}

	public Account deleteAccount(long id) {
		IAccountDAO dao = DAOFactory.getAccountDAO();
		return dao.deleteAccount(id);
	}

	public void updateAccount(Account account) {
		IAccountDAO dao = DAOFactory.getAccountDAO();
		dao.updateAccount(account);
	}
}
