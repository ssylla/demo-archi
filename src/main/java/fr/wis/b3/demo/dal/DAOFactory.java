package fr.wis.b3.demo.dal;

import fr.wis.b3.demo.dal.inmemory.InMemoryAccountDAO;
import fr.wis.b3.demo.dal.jpa.JPAAccountDAO;

import java.util.ResourceBundle;

public final class DAOFactory {
	
	private static final String MODE;
	
	static {
		ResourceBundle bundle = ResourceBundle.getBundle( "app" );
		MODE = bundle.getString( "store.mode" );
	}
	
	private DAOFactory(){}//prenvents initialization
	
	public static IAccountDAO getAccountDAO (){
		
		IAccountDAO dao = null;
		switch ( MODE ) {
			case "JPA" :
				dao = new JPAAccountDAO();
				break;
			case "IN_MEMORY":
				dao = new InMemoryAccountDAO();
				break;
		}
		return dao;
	}
}
