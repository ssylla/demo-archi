package fr.wis.b3.demo.dal;

import fr.wis.b3.demo.bo.Account;
import java.util.Set;
public interface IAccountDAO {

	void addAccount( Account account );
	Set<Account> findAllAccounts();
	Account findById(long id);
	void updateAccount(Account account);
	Account deleteAccount(long id);
}
