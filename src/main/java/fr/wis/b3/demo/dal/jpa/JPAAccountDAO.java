package fr.wis.b3.demo.dal.jpa;

import fr.wis.b3.demo.bo.Account;
import fr.wis.b3.demo.dal.IAccountDAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Set;

public class JPAAccountDAO implements IAccountDAO {
	
	private static final EntityManagerFactory EMF;
	
	static {
		EMF = Persistence.createEntityManagerFactory( "pu-bank" );
	}
	
	@Override
	public void addAccount( Account account ) {
		EntityManager entityManager = EMF.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist( account );
		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
	@Override
	public Set<Account> findAllAccounts() {
		return null;
	}
	
	@Override
	public Account findById( long id ) {
		return null;
	}
	
	@Override
	public void updateAccount( Account account ) {
	
	}
	
	@Override
	public Account deleteAccount( long id ) {
		return null;
	}
}
