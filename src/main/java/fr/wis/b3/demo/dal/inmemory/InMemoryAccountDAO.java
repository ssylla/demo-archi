package fr.wis.b3.demo.dal.inmemory;

import fr.wis.b3.demo.bo.Account;
import fr.wis.b3.demo.bo.SavingAccount;
import fr.wis.b3.demo.bo.SimpleAccount;
import fr.wis.b3.demo.dal.IAccountDAO;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class InMemoryAccountDAO implements IAccountDAO {
	
	private static final Map<Long, Account> ACCOUNTS;
	
	static {
		ACCOUNTS = new HashMap<>();
		ACCOUNTS.put( 1L, new SimpleAccount( 1L, 250 ) );
		ACCOUNTS.put( 2L, new SavingAccount( 2L, 4500 ) );
	}
	
	@Override
	public void addAccount( Account account ) {

		ACCOUNTS.put( account.getId(), account );
	}
	
	@Override
	public Set<Account> findAllAccounts() {
		Set<Account> accounts = new HashSet<>();
		for ( Map.Entry<Long, Account> item : ACCOUNTS.entrySet() ) {
			accounts.add( item.getValue() );
		}
		return accounts;
	}
	
	@Override
	public Account findById( long id ) {
		return ACCOUNTS.get(id);
	}
	
	@Override
	public void updateAccount( Account account ) {
		Account existingAccount = ACCOUNTS.get(account.getId());
		if (existingAccount != null) {
			ACCOUNTS.replace( account.getId(), account );
		}
	}
	
	@Override
	public Account deleteAccount(long id) {
		return ACCOUNTS.remove(id);
	}
}
