package fr.wis.b3.demo.ihm;

import fr.wis.b3.demo.bll.AccountService;
import fr.wis.b3.demo.bo.Account;
import fr.wis.b3.demo.bo.Address;
import fr.wis.b3.demo.bo.Agency;
import fr.wis.b3.demo.bo.SimpleAccount;

import java.util.Set;

public class App {
	public static void main( String[] args ) {
		
		addNewAccount();
		dspAccounts();
		dspAccount( 2L );
		deleteAccount( 2L );
		updateAccount( new Account( 3L, 1000 ) );
		dspAccount( 3L );
	}
	
	public static void addNewAccount() {
		AccountService service = AccountService.getSingle();
		SimpleAccount account = new SimpleAccount( 3L, 5200.0 );
		account.setAgency( new Agency("NTE44", new Address("11", "rue de l'arnaque", "44000", "Nantes") ) );
		service.createAccount( account );
	}
	
	public static void dspAccounts() {
		AccountService service = AccountService.getSingle();
		Set<Account> accounts = service.findAll();
		System.out.println( accounts );
	}
	
	public static void dspAccount( long id ) {
		AccountService service = AccountService.getSingle();
		Account accounts = service.findById( id );
		System.out.println( accounts );
	}
	
	public static void updateAccount( Account account ) {
		AccountService service = AccountService.getSingle();
		service.updateAccount( account );
		System.out.println( account );
	}
	
	public static void deleteAccount( long id ) {
		AccountService service = AccountService.getSingle();
		Account accounts = service.deleteAccount( id );
		System.out.println( accounts );
	}
}
