package fr.wis.b3.demo.bo;

import javax.persistence.Entity;

@Entity
public class SimpleAccount extends Account {
	
	private double overdraft;
	
	public SimpleAccount() {
	
	}
	
	public SimpleAccount( Long id, double balance) {
		super( id, balance );
	}
	
	public double getOverdraft() {
		return overdraft;
	}
	
	public void setOverdraft( double overdraft ) {
		this.overdraft = overdraft;
	}
}
