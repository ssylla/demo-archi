package fr.wis.b3.demo.bo;

import javax.persistence.Entity;

@Entity
public class SavingAccount extends Account {
	
	public SavingAccount() {}
	
	public SavingAccount( Long id, double balance ) {
		super( id, balance );
	}
}
