package fr.wis.b3.demo.bo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Account implements Serializable {
	
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	protected double balance;
	@ManyToOne(cascade = CascadeType.ALL)
	protected Agency agency;
	
	public Account() {}
	
	public Account( double balance ) {
		this.balance = balance;
	}
	
	public Account( Long id, double balance ) {
		this.id = id;
		this.balance = balance;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void setBalance( double balance ) {}
	
	public Agency getAgency() {
		return agency;
	}
	
	public void setAgency( Agency agency ) {
		if ( this.agency != null ) {
			this.agency.getAccounts().remove( this );
		}
		this.agency = agency;
		if ( this.agency != null ) {
			this.agency.getAccounts().add( this );
		}
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Account{" );
		sb.append( "id=" ).append( id );
		sb.append( ", balance=" ).append( balance );
		sb.append( ", agency=" ).append( agency );
		sb.append( '}' );
		return sb.toString();
	}
}
