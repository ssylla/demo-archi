package fr.wis.b3.demo.bo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Agency implements Serializable {
	
	private static int nbAgencies = 0;
	
	@Id
	@GeneratedValue
	private Long id;
	private String code;
	
	@Embedded
	private Address address;
	
	@OneToMany( mappedBy = "agency" )
	private Set<Account> accounts;
	
	public Agency() {
		nbAgencies++;
		address = new Address();
		accounts = new HashSet<>();
	}
	
	public Agency( String code, Address address ) {
		this();
		this.code = code;
		this.address = address;
	}
	
	public Agency( Long id, String code, Address address ) {
		this();
		this.id = id;
		this.code = code;
		this.address = address;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode( String code ) {
		this.code = code;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress( Address address ) {
		this.address = address;
	}
	
	public Set<Account> getAccounts() {
		return accounts;
	}
	
	public void setAccounts( Set<Account> accounts ) {
		this.accounts = accounts;
	}
	
	public void addAccount( Account account ) {
		account.setAgency( this );
	}
	
	public void removeAccount( Account account ) {
		account.setAgency( null );
	}
	
	public int getNbAgencies() {
		return nbAgencies;
	}
	
	public void setNbAgencies( int nbAgencies ) {
		this.nbAgencies = nbAgencies;
	}
	
	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer( "Agency{" );
		sb.append( "id=" ).append( id );
		sb.append( ", code='" ).append( code ).append( '\'' );
		sb.append( ", address=" ).append( address );
		sb.append( '}' );
		return sb.toString();
	}
}